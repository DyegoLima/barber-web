import styled, { css } from 'styled-components';
import Tooltip from '../Tooltip';

interface ContainerProps {
  isFocused: boolean;
  isFilled: boolean;
  isErrored: boolean;
}

export const Container = styled.div<ContainerProps>`
  background: #232129;
  border-radius: 10px;
  padding: 16px;
  width: 100%;
  display: flex;
  align-items: center;

  border: 2px solid #232129;
  color: #666360;

  ${props =>
    props.isErrored &&
    css`
      border-color: ${props.theme.color.border};
    `}

  ${props =>
    props.isFocused &&
    css`
      color: ${props.theme.color.primary};
      border-color: ${props.theme.color.primary};
    `}

  ${props =>
    props.isFilled &&
    css`
      color: ${props.theme.color.primary};
    `}



  & + div {
    margin-top: 8px;
  }

  input {
    background: transparent;
    flex: 1;
    border: 0;
    color: #f4ede8;

    &::placeholder {
      color: #666360;
    }

    svg {
      margin-right: 16px;
    }
  }
`;

export const Error = styled(Tooltip)`
  height: 20px;
  margin-left: 16px;

  span {
    background: ${props => props.theme.color.border};
    color: ${props => props.theme.color.white};

    &::before {
      border-color: ${props => props.theme.color.border} transparent;
    }
  }
`;
