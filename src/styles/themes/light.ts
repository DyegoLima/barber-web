export default {
  title: 'light',

  color: {
    primary: '#ff9000',
    secundary: '#f4ede8',

    white: '#fff',
    background: '#312E38',
    border: '#c53030',
  },
};
