export default {
  title: 'dark',

  color: {
    primary: '#f4ede8',
    secundary: '#ff9000',

    white: '#312E38',
    background: '#FFF',
    border: '#c53030',
  },
};
