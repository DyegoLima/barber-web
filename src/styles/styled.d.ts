import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    title: stirng;

    color: {
      primary: string;
      secundary: string;
      white: string;
      background: string;
      border: string;
    };
  }
}
