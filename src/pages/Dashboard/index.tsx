import { useAuth } from '../../hooks/AuthContext';

const Dashboard: React.FC = () => {
  const { signOut } = useAuth();
  const handleClick = (): void => {
    signOut();
  };
  return (
    <>
      <h1>Dashboard</h1>
      <button type="button" onClick={handleClick}>
        Logout
      </button>
    </>
  );
};
export default Dashboard;
